CREATE TABLE [dbo].[URM_COMPETITOR_RETAIL_DATA]
(
[Price_Survey_Area] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Size_Description] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Item_Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Item_Number] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_1] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_1] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_1] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_2] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_2] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_2] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_3] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_3] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_3] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_4] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_4] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_4] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_4] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_5] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_5] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_5] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_5] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_6] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_6] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_6] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_6] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_7] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_7] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_7] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_7] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_8] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_8] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_8] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_8] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_9] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_9] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_9] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_9] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_10] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_10] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_10] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_10] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple_11] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_11] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Price_Flag_11] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Change_Indicator_11] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reserved] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPC_EAN13] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReservedCol] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileDate] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JOB_FK] [bigint] NULL,
[product_structure_fk] [bigint] NULL,
[Data_value_fk] [bigint] NULL,
[Import_Timestamp] [datetime] NULL
) ON [PRIMARY]
GO
