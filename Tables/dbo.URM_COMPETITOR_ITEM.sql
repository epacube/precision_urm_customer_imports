CREATE TABLE [dbo].[URM_COMPETITOR_ITEM]
(
[COMP_CDE] [int] NULL,
[ITEM_NBR] [int] NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMP_ITEM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESC_30A] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPC_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LVLID_PAR] [int] NULL,
[CLSPARITM] [int] NULL,
[ITEM_SURR] [int] NULL,
[UPC_SYS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPC_11P0] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_IDENT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
