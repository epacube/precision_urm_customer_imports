CREATE TABLE [dbo].[URM_CUSTOMER_ATTRIBUTES]
(
[CUSTOMER] [int] NULL,
[DSDLGFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADRPTFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DSDBERBTH] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SNDCPNIMS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALWTPRCP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALWADNLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALWCPNLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USE_BB] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USESILFMT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RMT_RPT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPT_SEQ] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPCHGPCT] [float] NULL,
[SIL_VER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USEACTITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DLTITMMVT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DLTITMDAY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCLSUPFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SNDFUTALW] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCANWHSTG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCANDSDTG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCANWKS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCANCSUT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USEALTDSC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USEBRKITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NWTAGDAYS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FUTALWDAY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMMTAGLOC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VNDITMTAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPCDLYWKY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAGBYDPT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMMIMPTPR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAGVITCHG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MNTSPCTAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NOTAGUNA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPLTAGSPC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NWTAGRTL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAGDPTCHG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAGBYLOC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USEOLDROR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USENEWROR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAGCOMP1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAGCOMP2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPTAGTY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DSDCSTZNE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [int] NULL,
[TME_CRT] [int] NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
