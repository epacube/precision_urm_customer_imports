CREATE TABLE [dbo].[URM_CUSTOMER_ITEM_POS]
(
[CUSTOMER] [int] NULL,
[ITEM_NBR] [int] NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCL_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QTY_REQ] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QTY_PROH] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPT_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BOTDEPAMT] [float] NULL,
[BOTDEPDPT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIX_MATCH] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VIS_VER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPN_FLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
