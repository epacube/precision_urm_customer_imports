CREATE TABLE [dbo].[URM_ITEM_CLASS]
(
[LVLID_ITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLSID_ITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESC_30A] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LVLID_PAR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLSPARITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LVLID_NEW] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLSIDITMN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[Job_FK] [bigint] NULL
) ON [PRIMARY]
GO
