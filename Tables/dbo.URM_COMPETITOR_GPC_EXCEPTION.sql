CREATE TABLE [dbo].[URM_COMPETITOR_GPC_EXCEPTION]
(
[PRCSURCMP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMP_CDE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMP_UPC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFF_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRC_MULT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRICE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPCPRCFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMP_ITEM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
