CREATE TABLE [dbo].[URM_CUSTOMER_ITEM_LOCATION]
(
[CUSTOMER] [int] NULL,
[ITEM_NBR] [int] NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEQ_NUM1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHFTAGQTY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMLOCASL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMLOCSID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMLOCSEC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMLOCSHF] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMLOCPOS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMLOCFAC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAG_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAG_SPEC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAG_AD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAG_CPN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [int] NULL,
[TME_CRT] [int] NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [int] NULL,
[TME_UPD] [int] NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
