CREATE TABLE [dbo].[URM_COMPETITOR_GPC]
(
[COMP_UPC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPC_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CMPUPCDSC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPSZDSC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REVIEWED] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMP_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JOB_FK] [bigint] NULL,
[Import_Timestamp] [datetime] NULL
) ON [PRIMARY]
GO
