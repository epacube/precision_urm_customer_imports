CREATE TABLE [dbo].[URM_ZONE_PRICE_POINTS]
(
[ZONE_NUM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LVLID_ITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLSID_ITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRC_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RNDLOWLMT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RNDUPLMT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RNDAMT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[Job_FK] [bigint] NULL
) ON [PRIMARY]
GO
