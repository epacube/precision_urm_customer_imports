CREATE TABLE [dbo].[URM_COMPETITOR_ITEM_PRICE]
(
[COMP_CDE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFF_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRC_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRC_MULT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRICE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPCPRCFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JOB_FK] [bigint] NULL,
[Import_Timestamp] [datetime] NULL
) ON [PRIMARY]
GO
