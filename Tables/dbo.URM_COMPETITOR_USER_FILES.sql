CREATE TABLE [dbo].[URM_COMPETITOR_USER_FILES]
(
[Competitor_Code] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URM_Item_Surrogate] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Type_Code] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Multiple] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Price_Amount] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comp_User] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product_Structure_fk] [bigint] NULL,
[Comp_Entity_Structure_fk] [bigint] NULL,
[JOB_FK] [bigint] NULL,
[Import_Timestamp] [datetime] NULL
) ON [PRIMARY]
GO
