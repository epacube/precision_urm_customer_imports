CREATE TABLE [dbo].[URM_CUSTOMER_RORC_ATTRIBUTES]
(
[CUSTOMER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NEWITMFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHGITMFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DLTITMFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DSDITMFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLUCDEITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SNDCPMROR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RORCCSTZN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RORCRTLZN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URMDFTVND] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DSDDEFVND] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RORCMSTCT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDRORSEL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SPCRORSEL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
