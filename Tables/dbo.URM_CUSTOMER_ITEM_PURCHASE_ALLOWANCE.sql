CREATE TABLE [dbo].[URM_CUSTOMER_ITEM_PURCHASE_ALLOWANCE]
(
[CUSTOMER] [int] NULL,
[ITEM_NBR] [int] NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEAL_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFF_DATE] [int] NULL,
[TERM_DATE] [int] NULL,
[ALLOW_AMT] [float] NULL,
[ALLOW_TYP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALLOW_SRC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AD_PROMO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WHSERCDID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RTL_SRC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [int] NULL,
[TME_CRT] [int] NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [int] NULL,
[TME_UPD] [int] NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
