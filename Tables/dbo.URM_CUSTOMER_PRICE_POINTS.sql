CREATE TABLE [dbo].[URM_CUSTOMER_PRICE_POINTS]
(
[CUSTOMER] [int] NULL,
[LVLID_ITM] [int] NULL,
[CLSID_ITM] [int] NULL,
[PRC_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RNDLOWLMT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RNDUPLMT] [float] NULL,
[RNDAMT] [float] NULL,
[DTE_CRT] [int] NULL,
[TME_CRT] [int] NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [int] NULL,
[TME_UPD] [int] NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
