CREATE TABLE [dbo].[URM_COMPETITORS]
(
[COMP_CDE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDR1] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY_18A] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP_CODE] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_ABBR] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHT_NAME] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE_ID] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMP_TYPE] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JOB_FK] [bigint] NULL,
[Import_Timestamp] [datetime] NULL,
[Comp_entity_structure_fk] [bigint] NULL
) ON [PRIMARY]
GO
