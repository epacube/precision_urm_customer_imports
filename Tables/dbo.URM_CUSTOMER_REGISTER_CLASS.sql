CREATE TABLE [dbo].[URM_CUSTOMER_REGISTER_CLASS]
(
[RCCUST] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCRECL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCDS45] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCSCFL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCQREQ] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCMMCD] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCFSFL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCTXFL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCBDDP] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCBDEP] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCBDEC] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCAVER] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCDSCO] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCWEFL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCTAEN] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCTCOD] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCCOFL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCPENT] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCVIVY] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCT2FL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCT3FL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCT4FL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCQPRO] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCFSAF] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCCRDT] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCCRTM] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCCRUS] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCUPDT] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCUPTM] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RCUPUS] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
