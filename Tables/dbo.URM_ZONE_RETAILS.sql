CREATE TABLE [dbo].[URM_ZONE_RETAILS]
(
[ZONE_NUM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRC_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EFF_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TERM_DATE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRC_MULT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRICE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TM_PCT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COST_UNIT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RTL_STS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCCHGRSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[Job_FK] [bigint] NULL
) ON [PRIMARY]
GO
