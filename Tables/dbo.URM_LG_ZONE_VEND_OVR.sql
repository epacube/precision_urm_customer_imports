CREATE TABLE [dbo].[URM_LG_ZONE_VEND_OVR]
(
[FGRZON] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGITTK] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGVNDB] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGPCCA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGLGID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGREDY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGUIMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGCRDT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGCRTM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGCRUS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGUPDT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGUPTM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FGUPUS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
