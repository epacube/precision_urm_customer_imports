CREATE TABLE [dbo].[URM_CUSTOMER_ITEM]
(
[CUSTOMER] [int] NULL,
[ITEM_NBR] [int] NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMTRMDTE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIX_ABC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHF_TG_PT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TM_PCT] [float] NULL,
[LG_FLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STS_RIMAU] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_STS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LSTSHFTAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POS_DEPT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LINK] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LVLID_PAR] [int] NULL,
[CLSPARITM] [int] NULL,
[DISC_RSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VND_ALIAS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FS_FLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TX_FLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [int] NULL,
[TME_CRT] [int] NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_uci_1] ON [dbo].[URM_CUSTOMER_ITEM] ([IMPORT_TIMESTAMP], [JOB_FK]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
