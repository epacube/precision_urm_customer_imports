CREATE TABLE [dbo].[URM_LG_CUST_VEND_OVR]
(
[FFCUST] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFITTK] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFVNDB] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFPCCA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFLGID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFREDY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFUIMA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFCRDT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFCRTM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFCRUS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFUPDT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFUPTM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FFUPUS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
