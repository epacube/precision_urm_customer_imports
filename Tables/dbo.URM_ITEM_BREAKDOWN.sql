CREATE TABLE [dbo].[URM_ITEM_BREAKDOWN]
(
[ITEM_NBR] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BRKDWNITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BRKDITMTY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BRKDWNPCK] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[Job_FK] [bigint] NULL
) ON [PRIMARY]
GO
