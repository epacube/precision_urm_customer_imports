CREATE TABLE [dbo].[URM_ZONE_ITEM_CLASS]
(
[ZONE_NUM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LVLID_ITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLSID_ITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LG_FLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TM_PCT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D0] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D5] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D6] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D7] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D8] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCADJ_D9] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REQCHGAMT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REQCHGPCT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[Job_FK] [bigint] NULL
) ON [PRIMARY]
GO
