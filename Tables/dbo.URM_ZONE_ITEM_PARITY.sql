CREATE TABLE [dbo].[URM_ZONE_ITEM_PARITY]
(
[ZONE_NUM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMPARTYP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARITEM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARITMTYP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARLSSAMT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARLSSPCT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARMULT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product_structure_fk] [bigint] NULL,
[Child_Product_structure_fk] [bigint] NULL,
[Zone_entity_structure_fk] [bigint] NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[Job_FK] [bigint] NULL
) ON [PRIMARY]
GO
