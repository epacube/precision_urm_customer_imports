CREATE TABLE [dbo].[PRICESHEET_RETAIL_CHANGES_FROM_PRODUCTION]
(
[PRICE_MGMT_TYPE_CR_FK] [bigint] NULL,
[ITEM_CHANGE_TYPE_CR_FK] [int] NULL,
[QUALIFIED_STATUS_CR_FK] [int] NULL,
[PRICESHEET_BASIS_VALUES_FK] [bigint] NULL,
[ENTITY_ID] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENTITY_NAME] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENTITY_CLASS_CR_FK] [int] NULL,
[ITEM_DESCRIPTION] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALIAS_ID] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[cost_change_effective_date_cur] [date] NULL,
[COST_CHANGE_EFFECTIVE_DATE] [date] NULL,
[STORE_PACK] [int] NULL,
[case_cost_cur] [numeric] (18, 6) NULL,
[unit_cost_cur] [numeric] (18, 6) NULL,
[case_cost_new] [numeric] (18, 6) NULL,
[unit_cost_new] [numeric] (18, 6) NULL,
[PRICE_MULTIPLE_CUR] [int] NULL,
[PRICE_MULTIPLE_NEW] [int] NULL,
[PRICE_CUR] [money] NULL,
[PRICE_NEW] [numeric] (18, 2) NULL,
[GM_PCT_CUR] [numeric] (18, 4) NULL,
[GM_PCT_NEW] [numeric] (18, 4) NULL,
[PRICE_EFFECTIVE_DATE_CUR] [date] NULL,
[PRICE_CHANGE_EFFECTIVE_DATE] [date] NULL,
[COST_CHANGE] [numeric] (19, 6) NULL,
[PRICE_CHANGE] [numeric] (21, 4) NULL,
[ind_Margin_Tol] [int] NULL,
[ftn_Cost_Delta] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TARGET_MARGIN] [numeric] (18, 6) NULL,
[LEVEL_GROSS] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MINAMT] [money] NULL,
[DAYSOUT] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRODUCT_STRUCTURE_FK] [bigint] NULL,
[ENTITY_STRUCTURE_FK] [bigint] NULL,
[SUBGROUP_DESCRIPTION] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARITY_PL_PARENT_STRUCTURE_FK] [bigint] NULL,
[USE_LG_PRICE_POINTS] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[price_initial] [numeric] (18, 2) NULL,
[price_rounded] [numeric] (18, 2) NULL,
[DIGIT_DN_FK] [numeric] (18, 2) NULL,
[DIGIT_ADJ] [numeric] (18, 2) NULL,
[SRP_ADJUSTED] [money] NULL,
[GM_ADJUSTED] [numeric] (18, 4) NULL,
[TM_ADJUSTED] [numeric] (18, 4) NULL,
[HOLD_CUR_PRICE] [int] NULL,
[REVIEWED] [int] NULL,
[UPDATE_TIMESTAMP] [datetime] NULL,
[CREATE_TIMESTAMP] [datetime] NULL,
[UPDATE_USER_PRICE] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_USER_SET_TM] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_USER_HOLD] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_USER_REVIEWED] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARITY_DISCOUNT_PCT] [numeric] (18, 4) NULL,
[SUBGROUP_DATA_VALUE_FK] [bigint] NULL,
[IND_COST_CHANGE_ITEM] [int] NULL,
[STAGE_FOR_PROCESSING_FK] [bigint] NULL,
[IND_MARGIN_CHANGES] [int] NULL,
[IND_PARITY] [int] NULL,
[IND_RESCIND] [int] NULL,
[INSERT_STEP] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Pricing_Steps] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COST_CHANGE_REASON] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SIZE_UOM] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ftn_Breakdown_Parent] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALIAS_DESCRIPTION] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUBGROUP_ID] [bigint] NULL,
[ALIAS_DATA_VALUE_FK] [bigint] NULL,
[ITEM] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_GROUP] [bigint] NULL,
[parity_alias_id] [bigint] NULL,
[comments_rescind] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARITY_STATUS] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_SORT] [bigint] NULL,
[ind_display_as_reg] [int] NULL,
[entity_sort] [int] NULL,
[lg_activation] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ind_parity_status] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[parity_parent_product_structure_fk] [bigint] NULL,
[parity_child_product_structure_fk] [bigint] NULL,
[parity_parent_item] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[parity_child_item] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[pricesheet_basis_values_fk_cur] [bigint] NULL,
[related_item_alias_id] [bigint] NULL,
[change_status] [int] NULL,
[previous_lg_date] [date] NULL,
[unique_cost_record_id] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[item_group_alias_item] [bigint] NULL,
[PRICESHEET_RETAIL_CHANGES_FROM_PRODUCTION_ID] [bigint] NOT NULL IDENTITY(1, 1),
[price_calc_insert_timestamp] [datetime] NULL,
[vendor_alias] [int] NULL,
[pricesheet_retail_changes_fk] [bigint] NULL,
[import_timestamp] [date] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PRICESHEET_RETAIL_CHANGES_FROM_PRODUCTION] ADD CONSTRAINT [PK_PR_PRICESHEET_RETAIL_CHANGES_FROM_PRODUCTION] PRIMARY KEY CLUSTERED  ([PRICESHEET_RETAIL_CHANGES_FROM_PRODUCTION_ID]) WITH (FILLFACTOR=80, STATISTICS_NORECOMPUTE=ON) ON [PRIMARY]
GO
