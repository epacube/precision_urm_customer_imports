CREATE TABLE [dbo].[URM_ZONE_ITEM]
(
[ZONE_NUM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMTRMDTE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TM_PCT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LG_FLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_STS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LINK] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LVLID_PAR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLSPARITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISC_RSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VENDOR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[PRODUCT_STRUCTURE_FK] [bigint] NULL,
[ENTITY_STRUCTURE_FK] [bigint] NULL,
[Job_FK] [bigint] NULL
) ON [PRIMARY]
GO
