CREATE TABLE [dbo].[ztables_to_delete]
(
[tbl] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[records] [int] NULL,
[Drop_SQL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Select_SQL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Truncate_SQL] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Action] [int] NULL
) ON [PRIMARY]
GO
