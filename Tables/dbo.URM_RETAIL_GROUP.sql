CREATE TABLE [dbo].[URM_RETAIL_GROUP]
(
[RTL_GRP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESC_30A] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RTL_LEAD] [int] NULL,
[COST_ZONE] [int] NULL,
[DTE_CRT] [int] NULL,
[TME_CRT] [int] NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_IDENT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
