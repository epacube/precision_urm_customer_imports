CREATE TABLE [dbo].[URM_VENDOR]
(
[VENDOR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDR1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDR2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY_18A] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TELEPHONE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TELE_CNT2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAX_TEL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VND_TG_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONT_PERS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AC_EL_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FTP_ADDR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMAIL_ADR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WEB_SITE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
