CREATE TABLE [dbo].[URM_ITEM_NEW]
(
[ITEM_SURR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_NBR] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ATTFINFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCFINFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITMSURRPC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LVLID_ITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLSID_ITM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRODUCT_STRUCTURE_FK] [bigint] NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[Job_FK] [bigint] NULL
) ON [PRIMARY]
GO
