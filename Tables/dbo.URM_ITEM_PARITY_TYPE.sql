CREATE TABLE [dbo].[URM_ITEM_PARITY_TYPE]
(
[ITMPARTYP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESC_30A] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRIORITY] [int] NULL,
[DTE_CRT] [int] NULL,
[TME_CRT] [int] NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [int] NULL,
[TME_UPD] [int] NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_IDENT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
