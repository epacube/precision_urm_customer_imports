CREATE TABLE [dbo].[URM_CUSTOMER_VENDOR_RORC]
(
[B2CUST] [int] NULL,
[B2VNBA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[B2VERO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[B2CRDT] [int] NULL,
[B2CRTM] [int] NULL,
[B2CRUS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[B2UPDT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[B2UPTM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[B2UPUS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
