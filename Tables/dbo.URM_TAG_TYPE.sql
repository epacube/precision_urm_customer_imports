CREATE TABLE [dbo].[URM_TAG_TYPE]
(
[TAG_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESC_30A] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UNTPRCFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CSEPRCFLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEFFLG1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEFFLG2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SYSTAGTYP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTEISOCRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTEISOUPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
