CREATE TABLE [dbo].[URM_COMPETITOR_CHECKLIST_SEQUENCE]
(
[CHKLST_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPRPSQ1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPRPSQ2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPRPSQ3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPRPSQ4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPRPSQ5] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
