CREATE TABLE [dbo].[URM_COMPETITOR_PRICE_SURVEY]
(
[PRCSURCMP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRCSURARE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEQ_NUM3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CSTNEGAUT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMP_CDE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JOB_FK] [bigint] NULL,
[Import_Timestamp] [datetime] NULL
) ON [PRIMARY]
GO
