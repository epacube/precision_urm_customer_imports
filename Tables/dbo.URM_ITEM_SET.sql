CREATE TABLE [dbo].[URM_ITEM_SET]
(
[ITEM_SURR] [int] NOT NULL,
[ITEM_STS] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_01] ON [dbo].[URM_ITEM_SET] ([ITEM_SURR]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
