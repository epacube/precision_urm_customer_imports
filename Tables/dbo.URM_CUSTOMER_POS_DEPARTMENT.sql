CREATE TABLE [dbo].[URM_CUSTOMER_POS_DEPARTMENT]
(
[CUSTOMER] [int] NULL,
[POS_DEPT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FS_FLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TX_FLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VIS_VER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCL_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPN_FLG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WIC_FLAG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISCOUNT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_CRT] [int] NULL,
[TME_CRT] [int] NULL,
[USR_CRT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DTE_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TME_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USR_UPD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IMPORT_TIMESTAMP] [datetime] NULL,
[JOB_FK] [bigint] NULL
) ON [PRIMARY]
GO
