SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gary Stone
-- Create date: 2/14/2022
-- =============================================
CREATE PROCEDURE [utility].[test_for_pricesheet_retails_dups]
as
select * from INFORMATION_SCHEMA.columns where table_name = 'pricesheet_retails' and TABLE_SCHEMA = 'pricing'


drop table if exists #pr
select * into #pr from epacube_dev.pricing.pricesheet_retails

drop table if exists #pr_dups

select count(*) recs
, DATA_NAME_FK
, PRODUCT_STRUCTURE_FK
, ENTITY_STRUCTURE_FK
, RETAIL
, EFFECTIVE_DATE
, END_DATE
, AD_PROMO
, AD_ZONE_CODE
into #pr_dups
from epacube_dev.pricing.PRICESHEET_RETAILS
group by DATA_NAME_FK
, PRODUCT_STRUCTURE_FK
, ENTITY_STRUCTURE_FK
, RETAIL
, EFFECTIVE_DATE
, END_DATE
, AD_PROMO
, AD_ZONE_CODE
having count(*) > 1

--select top 50 *
--from #ssei_dups order by recs desc

--select data_name_fk, sum(recs) from #ssei_dups group by data_name_fk

create index idx_01 on #pr_dups(data_name_fk, entity_structure_fk, effective_date, product_structure_fk)

drop table if exists #dels

--select ss.*
--into #dels
--from epacube_dev.epacube.segments_settings_ei ss
--inner join #ssei_dups dups on ss.entity_structure_fk = dups.entity_structure_fk and ss.effective_date = dups.effective_date 
--		and ss.PROD_SEGMENT_FK = dups.prod_segment_fk and ss.data_name_fk = dups.data_name_fk

--select  
--dense_rank()over(partition by prod_segment_fk, entity_structure_fk, data_name_fk, effective_date, attached_entity_structure_fk
--, price_type_dv_fk, lower_limit, upper_limit, product_association_fk, sequence, parent_entity_data_value_fk
--order by segments_settings_ei_id) 'DRank', *
--into #deletes
--from #dels 

----select top 500 * from #deletes where isnull(sequence, 1) <> 1 order by prod_segment_fk, entity_structure_fk, effective_date

--create index idx_01 on #deletes(segments_settings_ei_id)
/*
delete ssei
from epacube_dev.epacube.segments_settings_ei ssei
inner join #deletes dels on ssei.segments_settings_ei_id = dels.SEGMENTS_SETTINGS_EI_ID and dels.DRank <> 1
*/
GO
