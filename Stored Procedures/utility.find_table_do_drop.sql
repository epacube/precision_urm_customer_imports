SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [utility].[find_table_do_drop]
as

drop table if exists dbo.ztables_to_delete
create table dbo.ztables_to_delete(tbl varchar(128) null, records int null, Drop_SQL varchar(256) null, Select_SQL varchar(256) null, Truncate_SQL varchar(256) null, Action int null)

declare @tbl varchar(128), @Sql varchar(max)--, @Drop_SQL varchar(256), @Select_SQL varchar(256)

		DECLARE Tables Cursor local for
			select table_catalog + '.' + table_schema + '.' + table_name tbl from epacube_dev.INFORMATION_SCHEMA.tables where (table_schema = 'dbo' or table_name like '%20%')
			and table_schema not in ('temptbls') and table_type = 'base table' and table_name <> 'ztables_to_delete'

			OPEN Tables;
			FETCH NEXT FROM Tables INTO @tbl
			WHILE @@FETCH_STATUS = 0

				Begin--Fetch

				Set @SQL = 'insert into dbo.ztables_to_delete(tbl, records, Drop_SQL, Select_SQL, Truncate_SQL)
				select ''' + @tbl + ''', ' + '(select count(*) from ' + @tbl + '), ''Drop Table ' + @tbl + ''', ''Select top 5000 * from ' + @tbl + ''''
				+ ', ''truncate table ' + @tbl + ''''
				exec(@SQL)

			FETCH NEXT FROM Tables INTO @tbl
				End--Fetch
			Close Tables;
			Deallocate Tables;

/*
	select * from dbo.ztables_to_delete order by records desc
	select sum(records) from dbo.ztables_to_delete
*/

/*
declare @tbl varchar(128) --, @Sql varchar(max) null
, @Action int, @Drop_SQL varchar(256), @Truncate_SQL varchar(256)--, @tbl varchar(256)

		DECLARE Cleaning Cursor local for
		select [action], [Drop_SQL], [Truncate_SQL], tbl from dbo.ztables_to_delete where isnull([action], 0) in (1, 2)	order by action desc


			OPEN Cleaning;
			FETCH NEXT FROM Cleaning INTO @Action, @Drop_SQL, @Truncate_SQL, @tbl
			WHILE @@FETCH_STATUS = 0

				Begin--Fetch

				If @Action = 1
				Begin
					exec(@Drop_SQL)
					exec('delete from dbo.ztables_to_delete where tbl = ''' + @tbl + '''')
				end
				else
				If @Action = 2
					exec(@truncate_SQL)

			FETCH NEXT FROM Cleaning INTO @Action, @Drop_SQL, @Truncate_SQL, @tbl
				End--Fetch
			Close Cleaning;
			Deallocate Cleaning;
*/
GO
